# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT


stages:
    - lint
    - test
    - run
    - deploy

# Define common settings for all jobs
default:
    image: python:3.9
    before_script:
        - pip install --upgrade pip
        - pip install poetry
        - poetry install

# Define a base test that will be used with different python versions
.base_test:
    stage: test
    script:
        - poetry run pytest tests/

license_compliance:
    stage: lint
    script:
        - poetry run reuse lint

code_style:
    stage: lint
    script:
        - poetry run black --check --diff .
        - poetry run isort --check --diff .

test:python:
    extends: .base_test
    image: python:${PYTHON_VERSION}
    parallel:
        matrix:
            - PYTHON_VERSION: ["3.8", "3.9", "3.10"]

run-job:
    stage: run
    script:
        - poetry run python -m astronaut_analysis
    rules:
        # only run this job if changes to the main branch are commited
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    artifacts:
        # pass results to the next job
        paths:
            - results/

pages:
    # use the default image of the runner
    stage: deploy
    before_script:
        - echo "Skipping before script"
    script:
        - mkdir public
        - cp results/age_histogram.png public/age_histogram.png
    rules:
        # only run this job if changes to the main branch are commited
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    dependencies:
        # use the results from the run job
        - run-job
    artifacts:
        paths:
            - public/